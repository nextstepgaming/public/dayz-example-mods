# DayZ Example Mods & Templates
A collection of example mods and templates for dayz free for anyone to use and adapt.

## License
Everything herein is licensed under Bohemia Interactive's DayZ Public License (DPL), please see [LICENSE.md](/LICENSE.md) for details.

![Bohemia Interactive's DayZ Public License (DPL)](/assets/DPL.png)

## Index
- [Snippets](https://gitlab.com/nextstepgaming/public/dayz-example-mods/-/snippets) - Useful snippets that show how to do certain things using scripting.
    - [Hide a Body Part](https://gitlab.com/nextstepgaming/public/dayz-example-mods/-/snippets/2368260)
    - [Teleport Player to Random Position](https://gitlab.com/nextstepgaming/public/dayz-example-mods/-/snippets/2368261)