# Basic Mod Structure
Here is a basic modding structure you can use to help you hit the ground running when creating a new mod.

### Worth Noteing
- The folder name is the "namespace" we will be using throughout. However, you can name the mod published to steam something a bit more meaningful.

- Standardizing the nomencalture is important, use all lowercase in files and folders, use camel case for class names, and snake case for class names.
    - lowercaseexample
    - CamelCaseExample
    - snake_case_example

- You should place a `$PBOPREFIX$.txt` and a `config.cpp` at the root of anything you want to be a PBO in the end. This will allow you to either select the top level mod folder and compile them all at once, or select any of the folders inside the top level and compile separately.

    Contents should look similar to:
    ```txt
    path\to\this\folder
    ```

### Scripts Mod
The `scripts` folder under your namespace or top level folder will hold all the scripts that augment the functionality of either game, mission, or world. You should use best effort to use the same folder structure below these as the DayZ developers did to stay organized.

### Objects Mod
If you look at the root of your `P:` drive you will see a `DZ` folder. Everything inside that folder I generally place inside of the `objects` folder. You could have a `DZ` folder inside of your mod, but to me it just makes sense to do it this way, because everyhting within is an asset.

- Any other category under the objects mod should follow the same pathing as the DayZ developers did, and should have it's own `config.cpp`.
- At the same level as the `config.cpp` you should also have a `data` folder that will contain any textures or RVMAT's.
- At the same level as the `config.cpp` you should have any relevent P3D files as well as their `model.cfg` or `nameofp3d.cfg` files.
- I highly recommend that you use proper class inheritence and dont override the deffinition of classes unless that us your intention.

### Batch Script Usages
- [nextstepgaming/link.bat](./nextstepgaming/link.bat) - This script will link create a namespace folder in your `P:` drive and simlink all folders of your mod into the namespace folder. This is so you can store your mods and edit them outside of the `P:` drive, but still be able to access proxies, properly load textures in Object Builder, successfully compile, etc. Additionally it will create a Workbench config file that can be used to load your mods into Workbench.
    - It assumes that your path to DayZ is `"C:\Program Files (x86)\Steam\steamapps\common\DayZ"`.
    - It can accept one postional argument of `true`, and if that is given, it will also link the mod to your DayZ installation so you can do filepatchiong. Not needed in most cases.
    - It assumes you have extracted DayZ data to a virtual drive `P:`. In most cases this is ok, but if you have used another drive letter for DayZ data, you should change the script to fit your needs.

    #### Usage:
    Simply double click the script after editing the path, or run it in the command prompt as seen below for filepatching.
    ```shell
    link.bat true
    ```

### Gotcha's
Some things that I wanted to add as "Got Ya" moments that I learned from experience and hours of fidgeting, so you dont have to.

- NEVER start a path with a slash. Yes, it will work in most cases, but when defining some things such as `hiddenSelectionMaterials` it will not work if the path starts with a slash.
