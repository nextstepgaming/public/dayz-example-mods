@echo off

setlocal enabledelayedexpansion enableextensions

SET local=%1

@REM Edit this for the correct path of your DayZ installation.
SET dayzRoot="C:\Program Files (x86)\Steam\steamapps\common\DayZ"

cd /D %~dp0

for %%a in ("%cd%") do set "root=%%~na"

:: P Drive Setup
IF NOT exist P:\%root%\ (
	ECHO Creating folder P:\%root%\
	mkdir P:\%root%
)

:: Link all mod folder
FOR /f %%d IN ('dir /B /AD') DO (
    SET mod=%%d
    CALL :LINK
)

:: Creating Workbench Launcher
CALL :GPROJ

CALL :DONE

:LINK
:: Unlink current mod folder
IF exist P:\%root%\%mod%\ (
	ECHO Removing existing link P:\%root%\%mod%
	rmdir P:\%root%\%mod%\
)

:: Link mod to current folder
ECHO Creating link P:\%root%\%mod%
mklink /J P:\%root%\%mod%\ %cd%\%mod%\

IF "%local%" == "true" (
    :: Local Mod Setup
    IF exist %dayzRoot%\@%root%\ (
        ECHO Removing existing link %dayzRoot%\@%root%
        rmdir %dayzRoot%\@%root%\
    )

    ECHO Creating link %dayzRoot%\@%root%
    mklink /J %dayzRoot%\@%root%\ P:\%root%\
)
EXIT /B 0

:GPROJ
> "%~dp0dayz.gproj" (
    @ECHO.GameProjectClass {
    @ECHO.	ID "DayZ"
    @ECHO.	TITLE "DayZ"
    @ECHO.	Configurations {
    @ECHO.		GameProjectConfigClass PC {
    @ECHO.			platformHardware PC
    @ECHO.			skeletonDefinitions "DZ/Anims/cfg/skeletons.anim.xml"
    @ECHO.			FileSystem {
    @ECHO.				FileSystemPathClass {
    @ECHO.				Name "Game Root"
    @ECHO.				Directory "P:"
    @ECHO.				}			
    @ECHO.			}
    @ECHO.			imageSets {
    @ECHO.				"gui/imagesets/ccgui_enforce.imageset"
    @ECHO.				"gui/imagesets/rover_imageset.imageset"
    @ECHO.				"gui/imagesets/dayz_gui.imageset"
    @ECHO.				"gui/imagesets/dayz_crosshairs.imageset"
    @ECHO.				"gui/imagesets/dayz_inventory.imageset"
    @ECHO.				"gui/imagesets/inventory_icons.imageset"
    @ECHO.				"gui/imagesets/main_menu_newsfeed.imageset"
    @ECHO.				"gui/imagesets/smart_panel.imageset"
    @ECHO.				"gui/imagesets/GUI_back_alpha.imageset"
    @ECHO.				"gui/imagesets/GUI_back_alpha_icon.imageset"
    @ECHO.				"gui/imagesets/xbox_buttons.imageset"
    @ECHO.				"gui/imagesets/playstation_buttons.imageset"
    @ECHO.				"gui/imagesets/selection.imageset"
    @ECHO.				"gui/imagesets/console_toolbar.imageset"
    @ECHO.			}
    @ECHO.			widgetStyles {
    @ECHO.				"gui/looknfeel/dayzwidgets.styles" 
    @ECHO.				"gui/looknfeel/widgets.styles"
    @ECHO.			}
    @ECHO.			ScriptModules {
    @ECHO.				ScriptModulePathClass {
    @ECHO.					Name "core"
    @ECHO.					Paths {
    @ECHO.						"scripts/1_Core"
    @ECHO.					}
    @ECHO.					EntryPoint ""
    @ECHO.				}
    @ECHO.				ScriptModulePathClass {
    @ECHO.					Name "gameLib"
    @ECHO.					Paths {
    @ECHO.						"scripts/2_GameLib"
    @ECHO.					}
    @ECHO.					EntryPoint ""
    @ECHO.				}
    @ECHO.				ScriptModulePathClass {
    @ECHO.					Name "game"
    @ECHO.					Paths {
    @ECHO.						"scripts/3_Game"
    @ECHO.						"%root:"=%/scripts/3_Game"
    @ECHO.					}
    @ECHO.					EntryPoint "CreateGame"
    @ECHO.				}
    @ECHO.				ScriptModulePathClass {
    @ECHO.					Name "world"
    @ECHO.					Paths {
    @ECHO.						"scripts/4_World"
    @ECHO.						"%root:"=%/scripts/4_World"
    @ECHO.					}
    @ECHO.					EntryPoint ""
    @ECHO.				}
    @ECHO.				ScriptModulePathClass {
    @ECHO.					Name "mission"
    @ECHO.					Paths {
    @ECHO.						"scripts/5_Mission"
    @ECHO.						"%root:"=%/scripts/5_Mission"
    @ECHO.					}
    @ECHO.					EntryPoint "CreateMission"
    @ECHO.				}
    @ECHO.				
    @ECHO.				ScriptModulePathClass {
    @ECHO.				Name "workbench"
    @ECHO.					Paths {
    @ECHO.						"scripts/editor/Workbench"
    @ECHO.						"scripts/editor/plugins"
    @ECHO.					} 
    @ECHO.					EntryPoint ""
    @ECHO.				} 
    @ECHO.			}
    @ECHO.		}
    @ECHO.		GameProjectConfigClass XBOX_ONE {
    @ECHO.			platformHardware XBOX_ONE
    @ECHO.		}
    @ECHO.		GameProjectConfigClass PS4 {
    @ECHO.			platformHardware PS4
    @ECHO.		}
    @ECHO.		GameProjectConfigClass LINUX {
    @ECHO.			platformHardware LINUX
    @ECHO.		}
    @ECHO.	}
    @ECHO.}
)
EXIT /B 0

:DONE
ECHO Done
PAUSE
EXIT /B 0
