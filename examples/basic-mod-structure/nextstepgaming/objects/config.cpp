class CfgPatches
{
	class NextStepGaming_Objects  // This should be path oriented starting with your namespace and delimited by an underscore.
	{
		requiredVersion = 0.1;
	};
};

class CfgMods
{
    class NextStepGaming_Objects  // I normally name this the same as the CfgPatches entry for simplicity.
    {
        type = "mod";
        dir = "nextstepgaming/objects";  // Should just be the path to the folder.

		// I dont generally fill in the below, because it will be handled at the mod.cpp level, so hide the images and name etc.
        name = "";
        version = "1.0";
        hideName = 1;
        hidePicture = 1;
	};
};
