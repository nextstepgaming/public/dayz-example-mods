class CfgPatches
{
    class NextStepGaming_Objects_Characters_Gloves  // This should be path oriented starting with your namespace and delimited by an underscore.
    {
        requiredVersion = 0.1;

        requiredAddons[] = {};  // The classes if any we want to inherit from. If retexturing this would be the the vanilla class we want to extend.

        units[] = {};  // Should be a list of classes you want to expose as public scope. eg. scope = 2
    };
};

class CfgVehicles
{
    // Place your modded or new objects inside of this block.
};