class CfgPatches
{
    class NextStepGaming_Objects_Characters_Tops  // This should be path oriented starting with your namespace and delimited by an underscore.
    {
        requiredVersion = 0.1;

        requiredAddons[] = {
            "DZ_Characters_Tops"  // The classes if any we want to inherit from. If retexturing this would be the the vanilla class we want to extend.
        };

        units[] = {
            "NSG_PCU5_Multicam_Snow"  // Should be a list of classes you want to expose as public scope. eg. scope = 2
        };
    };
};

class CfgVehicles
{
    // Below is a simple retecture example of a Tactical shirt which is a vanilla item.
    // We create a base class for a tactical shirt that inherits from the vanilla base class.
    // We then create a winter base class that inherits from our own color base and add isulation.
    // Then we create a new class name in the public scope for the shirt with references to the paa textures we created.

	// ##################################
	// # Tactical Shirt
	// ##################################
    class TacticalShirt_ColorBase;  // Base class of the item if you are retexturing.

    class NSG_TacticalShirt_Base: TacticalShirt_ColorBase  // Inherit the base class if needed, but also create your own base class to inherit from.
    {
        scope = 0;  // Base class scope should be private. eg. scope = 0 This ensures it cannot be spawned.
		itemsCargoSize[] = { 7, 6 };
		quickBarBonus = 2;
    };  // All classes that use this class will now have a quickbar bonus and cargo size as defined above.

    class NSG_TacticalShirt_Winter_Base: NSG_TacticalShirt_Base  // Inherit from the base class to create a winter base in this case.
    {
		varWetMax = 0.0;
		heatIsolation = 1.0;
    };  // All classes that use this as a base will now have "winter" insulation. The scope is private since it is inheriting from the previous base class.

    class NSG_PCU5_Multicam_Snow: NSG_TacticalShirt_Winter_Base  // Inherit from winter base so we have quickbar, cargo size, and insulation all included in this class.
	{
        scope = 2;  // Scope needs to be 2 if we are going to spawn this.
        displayName = "BNGP US Multicam Snow PCU5 Jacket";  // The name for this to be shown to the player.
        descriptionShort = "Part of the Bloody Nightmare US Multicam Snow Gear";  // The description to be shown to the player.


        // The hidden selection textures below are all the same, but respectively are for mal, femal, and ground models. You will get better results by retexturing each of these separately.
        // It is assumed these are already defined in tha parent class hiddenSelections block. ANd that assumes there are sections in the model.cfg. The model.cfg will reference selections defined in the P3D.
        hiddenSelectionsTextures[] = {  // The path to the PAA files to be used as replacement textures.
            "nextstepgaming\objects\characters\tops\data\tacticalshirt_multicam_snow_co.paa",
            "nextstepgaming\objects\characters\tops\data\tacticalshirt_multicam_snow_co.paa",
            "nextstepgaming\objects\characters\tops\data\tacticalshirt_multicam_snow_co.paa"
        };  // Notice no leading slashes!!!
    };

};