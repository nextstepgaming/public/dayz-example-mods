class CfgPatches
{
	class NextStepGaming_Scripts  // This should be path oriented starting with your namespace and delimited by an underscore.
	{
		requiredVersion = 0.1;

		requiredAddons[] = {  // here you should add any dependencies to other scripts you may be overriding or modding.
			"DZ_Scripts"
		};
	};
};

class CfgMods
{
    class NextStepGaming_Scripts  // I normally name this the same as the CfgPatches entry for simplicity.
    {
        type = "mod";
        dir = "nextstepgaming/scripts";  // Should just be the path to the folder.

		// I dont generally fill in the below, because it will be handled at the mod.cpp level, so hide the images and name etc.
        name = "";
        version = "1.0";
        hideName = 1;
        hidePicture = 1;

        dependencies[] = {  // Should include anything you are modding. I generally just include them all and below link to an empty folder if its unused.
			"Game",
			"World",
			"Mission"
		};

        class defs
        {
			class gameScriptModule  // List of files where your game script modules live. I generally add common folder as well, this will hold the includes.c which could be used for ifdef statements for debugging etc.
			{
				value="";
				files[] = {
					"nextstepgaming/scripts/common",
					"nextstepgaming/scripts/3_game"
				};
			};

			class worldScriptModule  // List of files where your world script modules live. I generally add common folder as well, this will hold the includes.c which could be used for ifdef statements for debugging etc.
			{
				value = "";
				files[] = {
					"nextstepgaming/scripts/common",
					"nextstepgaming/scripts/4_world"
				};
			};

			class missionScriptModule  // List of files where your mission script modules live. I generally add common folder as well, this will hold the includes.c which could be used for ifdef statements for debugging etc.
			{
				value = "";
				files[] = {
					"nextstepgaming/scripts/common",
					"nextstepgaming/scripts/5_mission"
				};
			};
		};
    };
};
